﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Console.ExamPrep.Interface
{
    interface IFlyable
    {
        void fly();
    }
}
