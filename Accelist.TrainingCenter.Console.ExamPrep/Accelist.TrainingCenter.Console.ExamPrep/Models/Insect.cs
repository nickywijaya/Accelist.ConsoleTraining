﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Console.ExamPrep.Models
{
    public abstract class Insect
    {
        public string Name { get; set; }

        public decimal Height { get; set; }

        public decimal Weight { get; set; }

        public string Species { get; set; }
    }
}
