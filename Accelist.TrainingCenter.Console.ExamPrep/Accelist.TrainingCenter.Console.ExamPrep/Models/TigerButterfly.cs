﻿using Accelist.TrainingCenter.Console.ExamPrep.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Console.ExamPrep.Models
{
    public class TigerButterfly : Butterfly , IFlyable
    {
        public void fly()
        {
            System.Console.WriteLine("it can fly");
        }
    }
}
