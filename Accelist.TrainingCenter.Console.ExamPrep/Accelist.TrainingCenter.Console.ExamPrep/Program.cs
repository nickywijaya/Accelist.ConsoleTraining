﻿using Accelist.TrainingCenter.Console.ExamPrep.Models;
using Accelist.TrainingCenter.Console.ExamPrep.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Console.ExamPrep
{
    class Program
    {
        public static void Main(string[] args)
        {
            MenuService.ShowInputMenu();
            
            System.Console.ReadKey();
        }
    }
}
