﻿using Accelist.TrainingCenter.Console.ExamPrep.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Console.ExamPrep.Service
{
    public static class MenuService
    {

        public static List<Butterfly> Butterflies = new List<Butterfly>();
        public static List<Beetle> Beetles = new List<Beetle>();

        public static void ShowInputMenu()
        {
            //numerical only
            System.Console.Write("Please enter special code : ");

            var input = System.Console.ReadLine();

            while (input != "12345")
            {
                System.Console.WriteLine("Invalid Special Code");
                System.Console.Write("Please enter special code : ");
                input = System.Console.ReadLine();
            }

            System.Console.Write("Please enter any number : ");

            var numericalInput = System.Console.ReadLine();
            var number = 0;
            while (!Int32.TryParse(numericalInput, out number))
            {
                System.Console.WriteLine("Not a number");
                System.Console.Write("Please enter any number : ");
                numericalInput = System.Console.ReadLine();
            }

            System.Console.Write(@"Your number was accepted.
press enter to continue...");
            ShowMainMenu();
        }

        public static void ShowMainMenu()
        {
            System.Console.WriteLine(@"Welcome to the Insect Paradise
Menu : 
1. Add Insect
2. View Insect
3. Release Insect
4. Exit
Please Input either 1 ,2 ,3, 4 : ");

            var input = System.Console.ReadLine();
            var numericalInput = 0;

            while (!Int32.TryParse(input, out numericalInput) || (numericalInput < 1 || numericalInput > 3))
            {
                System.Console.WriteLine("Invalid Option!");
                input = System.Console.ReadLine();
            }

            if (numericalInput == 1) ShowAddInsectMenu();
            if (numericalInput == 2) ShowInsectListMenu();
            if (numericalInput == 3) ShowInsectReleaseMenu();


        }

        public static void ShowAddInsectMenu()
        {

            #region Aneka Input


            System.Console.WriteLine(@"Add Insect : 
1. Tiger Butterfly
2. Hercules Beetle
3. Water Beetle
Please Input : ");

            System.Console.WriteLine();

            var input = System.Console.ReadLine();
            var numericalInput = 0;

            while (!Int32.TryParse(input, out numericalInput) || (numericalInput < 1 || numericalInput > 3))
            {
                System.Console.WriteLine("Invalid Option!");
                input = System.Console.ReadLine();
            }

            AddInsectService addInsectService = new AddInsectService();
            System.Console.Write("Name : ");
            var name = System.Console.ReadLine();

            //height
            System.Console.Write("Height : ");
            var height = System.Console.ReadLine();
            var heightDecimal = 0m;
            while (!decimal.TryParse(height, out heightDecimal))
            {
                System.Console.WriteLine("not a decimal value");
                height = System.Console.ReadLine();
            }

            //weight
            System.Console.Write("Weight : ");
            var weight = System.Console.ReadLine();
            var weightDecimal = 0m;

            while (!decimal.TryParse(height, out weightDecimal))
            {
                System.Console.WriteLine("not a decimal value");
                weight = System.Console.ReadLine();
            }

            #endregion


            if (input == "1")
            {
                TigerButterfly tigerbutterfly = new TigerButterfly
                {
                    Name = name,
                    Height = heightDecimal,
                    Weight = weightDecimal,
                    Species = "Tiger Butterfly"
                };
                addInsectService.AddTigerButterfly(tigerbutterfly);
            };
            if (input == "2")
            {
                HerculesBeetle herculesBeetle = new HerculesBeetle
                {
                    Name = name,
                    Height = heightDecimal,
                    Weight = weightDecimal,
                    Species = "Hercules Beetle"
                };
                addInsectService.AddHerculesBeetle(herculesBeetle);
            };
            if (input == "3")
            {
                WaterBeetle waterBeetle = new WaterBeetle
                {
                    Name = name,
                    Height = heightDecimal,
                    Weight = weightDecimal,
                    Species = "Water Beetle"
                };
                addInsectService.AddWaterBeetle(waterBeetle);
            };


            // ini sama kyk break aja, dia bakal exit dari fungsi kita
            //return;
            ShowInputMenu();
        }

        public static void ShowInsectListMenu()
        {

            //Butterflies
            if (Butterflies.Count == 0) System.Console.WriteLine("Not Butterflies Yet");
            else
            {
                System.Console.WriteLine("Butterflies");
                foreach (var item in Butterflies)
                {
                    System.Console.WriteLine($"Name : {item.Name}");
                    System.Console.WriteLine($"Weight : {item.Weight}");
                    System.Console.WriteLine($"Height : {item.Height}");
                    System.Console.WriteLine($"Species : {item.Species}");
                }
            }

            //Beetles
            if (Beetles.Count == 0) System.Console.WriteLine("Not Beetles Yet");
            else
            {
                System.Console.WriteLine("Beetles");
                foreach (var item in Beetles)
                {
                    System.Console.WriteLine($"Name : {item.Name}");
                    System.Console.WriteLine($"Weight : {item.Weight}");
                    System.Console.WriteLine($"Height : {item.Height}");
                    System.Console.WriteLine($"Species : {item.Species}");
                }
            }
            ShowInputMenu();
        }

        public static void ShowInsectReleaseMenu()
        {

            System.Console.Write(@"Choose What you want to delete : 
1. Butterflies
2. Beetle
Choose : ");
            var input = System.Console.ReadLine();

            if (Int32.Parse(input) == 1)
            {
                var indexButterflies = 0;
                //Butterflies
                if (Butterflies.Count == 0) System.Console.WriteLine("Not Butterflies Yet");
                else
                {
                    System.Console.WriteLine("Butterflies");
                    foreach (var item in Butterflies)
                    {
                        System.Console.WriteLine($"{indexButterflies + 1}. Name : {item.Name}");
                        indexButterflies++;
                    }
                }

                System.Console.Write("Pleace Choose The Number you want to delete : ");
                var x = System.Console.ReadLine();
                Butterflies.RemoveAt(Int32.Parse(x) - 1);
            }

            if (Int32.Parse(input) == 2)
            {
                var indexBeetle = 0;
                //Beetles
                if (Beetles.Count == 0) System.Console.WriteLine("Not Beetles Yet");
                else
                {

                    System.Console.WriteLine("Beetles");
                    foreach (var item in Beetles)
                    {
                        System.Console.WriteLine($"{indexBeetle + 1}. Name : {item.Name}");
                        indexBeetle++;
                    }
                }
                System.Console.Write("Pleace Choose The Number you want to delete : ");
                var x = System.Console.ReadLine();
                Beetles.RemoveAt(Int32.Parse(x) - 1);
            }

            ShowInputMenu();
        }
    }
}
