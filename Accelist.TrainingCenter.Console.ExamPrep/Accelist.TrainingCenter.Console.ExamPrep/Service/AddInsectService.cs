﻿using Accelist.TrainingCenter.Console.ExamPrep.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.Console.ExamPrep.Service
{
    public class AddInsectService
    {

        public void AddTigerButterfly(TigerButterfly tigerButterfly)
        {
            MenuService.Butterflies.Add(tigerButterfly);
        }


        public void AddHerculesBeetle(HerculesBeetle herculesBeetle)
        {
            MenuService.Beetles.Add(herculesBeetle);
        }

        public void AddWaterBeetle(WaterBeetle waterBeetle)
        {
            MenuService.Beetles.Add(waterBeetle);
        }

       
    }
}
