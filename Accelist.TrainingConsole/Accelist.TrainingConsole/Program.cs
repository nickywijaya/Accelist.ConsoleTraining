﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingConsole
{
    class Program
    {

    static void Main(string[] args)
        {
            #region array

            var ar = new int[5];


            #endregion

            #region linked list
            var x2 = new LinkedList<int>();
            x2.AddFirst(3);
            x2.AddFirst(5);
            var x3 = x2.AddFirst(7);
            x2.AddAfter(x3, 8);


            #endregion

            #region haystack
            var names = new List<string>
            {
                "daniel","mikael","george","Tie"
            };

            var haystack = new HashSet<string>(names);

            //search = 0(n);
            var contain = names.Contains("daniel");

            foreach (var item in haystack)
            {
                var condition = haystack.Contains(item);
                if(condition)
                {

                }
            }

            Console.WriteLine(contain);


            #endregion

            #region dictionary
            //binding dict ke kelas
            //var dict = new Dictionary<string, ClassOrang>();
            //dict["nicky"] = new ClassOrang { name = x2, phone=x3 };

            //nama , hp
            var dict = new Dictionary<string, string>();
            dict["nicky"] = "777";
            dict["nicky2"] = "7774";
            dict["nicky3"] = "7773";
            

            //lebih aman pakein aja try-catch biar kalo inputan gk dapet key dia kgk break
            try
            {
                var hp = dict["nickyss"];
                Console.WriteLine(hp);
            }
            catch(Exception e)
            {
                Console.WriteLine("data not found");
            }


            //nanti bisa maenan input aja key-nya sesuai search input dri program

            #endregion

            #region C5
            var c5 = new C5.IntervalHeap<int>();
            c5.Add(5); // O(log2 N)
            c5.Add(3);
            c5.Add(7);

            Console.WriteLine(ar.Min()); // 0(1)
            Console.WriteLine(ar.Max());

            #endregion

            #region parallel
            var x4 = new List<int>();

            Parallel.For(0, 10000, i =>
            {
                x4.Add(i);
            });

            #endregion

            #region anon method
            //<Param 1, Param2, return>
            Func<int, int, int> add = (a, b) =>
              {
                  return a + b;
              };
            add(5, 5);

            Action foo = () => { Console.WriteLine("hai"); };

            foo();



            #endregion

            #region LINQ
            var x5 = new List<int> { 1, 3, 4, 6, 7 };
            Console.WriteLine(x5.Sum());

            #endregion

            #region to dictionary via LINQ Ext Method
            //var dict = students.toDictionary(Q => Q.Id, Q => Q.Name);
            //dict[1] = name;
            #endregion

            #region MyRegion
            var x6 = new List<int> { 1, 2, 3, 4, 5 };

            //klo gk ada toList nanti dia bakal keluar 1,4,9,16,25,100 karena ada .Add dibawah.. dia itu jalanin ini ketika di panggil aja di foreach
            var query = x6.Select(Q => Q * Q).ToList();

            x6.Add(10);

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }
            #endregion




            Console.ReadKey();
        }
    }
}
